// -*-c++-*-
#pragma once

#include <string>
#include <cstdint>

// contains definition of prelude::digest function and common implementations
namespace prelude { 
    namespace  { // interior namespace to prevent external visibility
        // implementation of murmurhash2B for generic hash function
        static inline uint64_t digest(const void *ptr, size_t len) {
            const uint64_t seed  = 0x80FC33E7B36ED08Dull; // random seed
            const uint64_t mult  = 0xC6A4A7935BD1E995ull;
            const uint8_t  shift = 47;
            uint64_t       hash  = seed ^ (len*mult);

            const char *keys = (char*)ptr;
            for (; len >= 8; len -= 8, keys += 8) {
                uint64_t key = *(uint64_t*)keys;
                key  *= mult;
                key  ^= key >> shift;
                key  *= mult;
                hash ^= key;
                hash *= mult;
            }
            
            switch ( len & 7 ) { // fallthrough comments are required to silence warnings in g++7
                case 7: hash ^= (uint64_t)keys[6] << 48; // fall through
                case 6: hash ^= (uint64_t)keys[5] << 40; // fall through
                case 5: hash ^= (uint64_t)keys[4] << 32; // fall through
                case 4: hash ^= (uint64_t)keys[3] << 24; // fall through
                case 3: hash ^= (uint64_t)keys[2] << 16; // fall through
                case 2: hash ^= (uint64_t)keys[1] << 8;  // fall through
                case 1: hash ^= (uint64_t)keys[0];       // fall through
                        hash *= mult;
            }
            
            hash ^= hash >> shift;
            hash *= mult;
            hash ^= hash >> shift;
            return hash;
        }

        
        // pre-defined hash functions for built in types
        // numeric types just hash to their integral value.
        // This is an optimization inspired by Python.  Small consecutive
        // integers then distribute perfectly across the table.
        inline uint64_t constexpr digest(uint8_t     val) { return val; }
        inline uint64_t constexpr digest( int8_t     val) { return val; }
        inline uint64_t constexpr digest(uint16_t    val) { return val; }
        inline uint64_t constexpr digest( int16_t    val) { return val; }
        inline uint64_t constexpr digest(uint32_t    val) { return val; }
        inline uint64_t constexpr digest( int32_t    val) { return val; }
        inline uint64_t constexpr digest(uint64_t    val) { return val; }
        inline uint64_t constexpr digest( int64_t    val) { return val; }
        inline uint64_t           digest(std::string str) { return digest(str.c_str(), str.size()); }

        // variant that take a const char* as a constexpr, this allows compile-time compilation of
        // hash values for string literals.
        
        // default hash function for POD data
        template <typename T>
        inline uint64_t digest(const T& val) { 
            static_assert(std::is_standard_layout<T>::value, "default digest requires standard layout");
            return digest(&val, sizeof(val));
        }
    }
}
