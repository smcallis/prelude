// -*-c++-*-
#pragma once

// contains an implementation of a hashset which is basically a specialization of prelude::hashmap but
// without a value element.  So the container _only_ contains keys and allows for O(1) membership query
// There's enough differences between hashset and hashmap that a straight template specialization didn't make sense

#include <cstdlib>
#include <cstdint>

#include <limits>     // std::numeric_limits
#include <initializer_list>

#include <prelude/maybe.h>
#include <prelude/hashing.h>

namespace prelude {
    namespace  { // interior namespace to prevent external visibility
        template <class K>
        struct hashset {
            static const size_t EMPTY_BUCKET = std::numeric_limits<size_t>::max();
            static const size_t DEFAULT_SIZE = 8;            

            // convenience typedefs
            typedef K key_type;

            hashset();                                           // default constructor
            hashset(std::initializer_list<K> list);              // initializer list constructor
            hashset(const hashset&  other);                      // copy constructor
            hashset(      hashset&& other);                      // move constructor
           ~hashset();                                           // destructor

            hashset&  operator =(hashset other);                 // copy-and-swap assignment operator

            bool      operator ==(const hashset<K>&)  const;     // return true if two sets are equal
            bool      operator !=(const hashset<K>&)  const;     // return true if two sets are not equal

            hashset&  operator +=(const hashset<K>&);            // in-place union of sets
            hashset   operator + (const hashset<K>&)  const;     // out-of-place union of sets
            hashset&  operator +=(      hashset<K>&&);           // in-place union of sets with move semantics
            hashset   operator + (      hashset<K>&&) const;     // out-of-place union of sets with move semantics

            hashset&  operator -=(const hashset<K>&);            // in-place difference of sets
            hashset   operator - (const hashset<K>&)  const;     // out-of-place difference of sets

            bool        has    (const K&  key) const;            // return true if a given key is in the set

            hashset<K>& insert (      K&& key);                  // insert new key into the table
            hashset<K>& insert (const K&  key);

            void        remove (const K&  key);                  // erase key from the table if it exists
            void        clear  ();                               // clear the container of all entries
            void        compact();                               // rehash set to minimum size given current entries            

            bool        issubset  (const hashset<K> &) const;    // return true if this set is a subset of other
            bool        issuperset(const hashset<K> &) const;    // return true if this set is a superset of other
            
            bool        empty()    const { return size() == 0; } // return true if the set is empty
            size_t      size ()    const { return used_;       } // return current size of the set

        private:
            void        resize_(size_t);                         // function to resize the set
            ssize_t     search_(const K& key) const;             // search for a particular key in the set

            
            // modulo function for wrapping hash value back into bucket, we
            // limit ourselves to power-of-2 sizes, so we can use & instead of %
            static inline size_t modulo(size_t hash, size_t cap) {
                return hash & (cap-1);
            }

            
            // compute hash for instance of key, don't use the empty sentinel as a hash value
            static inline size_t hash_key(const K& key) {
                size_t hash = digest(key);
                if (hash == EMPTY_BUCKET) {
                    hash++;
                }
                return hash;
            }

            
            // bucket type, holds a hash, and a key
            // union type gives us aligned storage for key without initializing it.
            struct bucket {  
                bucket() : hash(EMPTY_BUCKET) {}
               ~bucket() {
                   // if bucket isn't empty, have to destruct key explicitly
                   if (hash != EMPTY_BUCKET) {
                       key.~K();
                   }
                }

                // anonymous union provides aligned, but uninitialized memory for key storage
                size_t hash;
                union {
                    K key;
                };
            };

            
            // compute displacement of a given hash found at index from its ideal location
            size_t probedist(size_t hash, size_t idx) const {
                size_t desired = modulo(hash, capacity_);

                if (desired <= idx) {
                    return idx - desired;
                } else {
                    // index wrapped around, return circular distance
                    return (capacity_ - desired) + idx;
                }
            }
            
            size_t  capacity_;
            size_t  used_;
            bucket *storage_;

            template <class KK> friend void swap(hashset<KK> &flip, hashset<KK> &flop);
        };


        //-------------------------------------------------------------------------------
        // hashset implementation
        //-------------------------------------------------------------------------------   
        // swap operation
        template <class K>
        void swap(hashset<K> &flip, hashset<K> &flop) {
            using std::swap;
            swap(flip.capacity_, flop.capacity_);
            swap(flip.used_,     flop.used_);
            swap(flip.storage_,  flop.storage_);
        }


        // default constructor 
        template <class K>
        hashset<K>::hashset()
            : capacity_(0), used_(0), storage_(nullptr) {}
        
        
        // destructor
        template <class K>
        hashset<K>::~hashset() {
            delete [] storage_; // storage destructor will handle destructing any keys we have
            storage_ = nullptr;
        }    

        
        // initializer list constructor
        template <class K>
        hashset<K>::hashset(std::initializer_list<K> list) : hashset() {
            for (auto item : list) {
                insert(*item);
            }
        }

    
        // copy constructor
        template <class K>
        hashset<K>::hashset(const hashset& other)
            : capacity_(other.capacity_), used_(other.used_), storage_(new bucket[capacity_]) {

            // have to construct keys via placement new rather than assigning them
            // because default constructor for bucket will not give us a valid object to assign to
            size_t cnt=0;
            for (size_t ii=0; ii < capacity_; ii++) {
                if (cnt == other.used_) break;
                
                size_t hash = other.storage_[ii].hash;
                if (hash != EMPTY_BUCKET) { 
                    storage_[ii].hash = hash;
                    new (&storage_[ii].key) K(other.storage_[ii].key);
                    cnt++;
                }
            }
        }
        

        // move constructor
        template <class K>
        hashset<K>::hashset(hashset&& other) {
            capacity_       = other.capacity_;
            used_           = other.used_;
            storage_        = other.storage_;
            other.capacity_ = 0;
            other.used_     = 0;
            other.storage_  = nullptr;
        }

        
        // assignment operator, uses copy-and-swap trick
        template <class K>
        hashset<K>& hashset<K>::operator =(hashset other) {
            swap(*this, other);
            return *this;
        }


        // equality operator between two sets
        template <class K>
        bool hashset<K>::operator ==(const hashset<K> &other) const {
            if (size() != other.size()) {
                return false;
            }

            bool   equal = true;
            size_t cnt   = 0;
            for (size_t ii=0; ii < capacity_; ii++) {
                if (cnt >= used_) break;

                if (storage_[ii].hash != EMPTY_BUCKET) {
                    equal &= other.has(storage_[ii].key);
                    cnt++;
                }
            }
            return equal;
        }

        template <class K>
        bool hashset<K>::operator !=(const hashset<K> &other) const {
            return !operator==(other);
        }
        

        // merge sets in-place
        template <class K>
        hashset<K>& hashset<K>::operator +=(const hashset<K> &other) {
            size_t cnt=0;
            for (size_t ii=0; ii < other.capacity_; ii++) {
                if (cnt >= other.used_) break;

                if (other.storage_[ii].hash != EMPTY_BUCKET) {
                    insert(other.storage_[ii].key);
                    cnt++;
                }
            }
            return *this;
        }

        
        // merge sets in-place with move semantics
        template <class K>
        hashset<K>& hashset<K>::operator +=(hashset<K>&& other) {
            size_t cnt=0;
            for (size_t ii=0; ii < other.capacity_; ii++) {
                if (cnt >= other.used_) break;

                if (other.storage_[ii].hash != EMPTY_BUCKET) {
                    insert(std::move(other.storage_[ii].key));
                    other.storage_[ii].hash  = EMPTY_BUCKET;
                    cnt++;
                }
            }
            return *this;
        }


        // merge sets out-of-place with move semantics
        template <class K>
        hashset<K> hashset<K>::operator + (hashset<K>&& other) const {
            // create temporary, reserve enough space to avoid very poor worst case collision behavior
            // if we let the container resize itself while doing this, we can get a scenario
            // were two disjoint sets (eg evens and odds) are added, and the first wraps around to low buckets
            // then when we add the second set in, we get very poor collision behavior until we resize again
            hashset<K> res;
            res.resize_(size() + other.size());

            // add sets together
            res += *this;   
            res += std::forward<hashset<K>>(other);
            return res;
        }


        // overload for set addition with lvalue parameter
        template <class K>
        hashset<K> hashset<K>::operator + (const hashset<K>& other) const {
            hashset<K> res;
            res.resize_(size() + other.size());

            // add sets together
            res += *this;   
            res += other;
            return res;
        }
        

        // remove elements of another set from this set to find difference
        template <class K>
        hashset<K>& hashset<K>::operator -=(const hashset<K> &other) {
            size_t cnt=0;
            for (size_t ii=0; ii < other.capacity_; ii++) {
                if (cnt >= other.used_) break;
                
                if (other.storage_[ii].hash != EMPTY_BUCKET) {
                    remove(other.storage_[ii].key);
                    cnt++;
                }
            }
            return *this;
        }


        // set difference but out of place
        template <class K>
        hashset<K> hashset<K>::operator - (const hashset<K> &other) const {
            hashset<K> res = *this;
            res -= other;
            return res;
        }
        
        
        // insert operation.  If key isn't a member of the set, it's added, otherwise no change.
        template <class K>
        hashset<K>& hashset<K>::insert(K&& key) {
            using std::swap; // for ADL
            using std::move;
            using std::forward;

            if (capacity_ == 0) {
                resize_(DEFAULT_SIZE);
            }
            
            size_t hash = hash_key(key);
            size_t idx  = modulo(hash, capacity_);
           
            // scan through container until we find an empty bucket.  If we find the key
            // along the way it already exists in the set and we're done.  If we find any elements
            // with a smaller probe distance than ours, swap with them and continue loop
            bool swapped=false;
            for (size_t ii=0, curdist=0;  ii < capacity_;  ii++, curdist++) {
                size_t pos = modulo(idx + ii, capacity_);

                if (storage_[pos].hash == EMPTY_BUCKET) {
                    // found an empty bucket, place what we currently have there
                    if (used_ >= 3*capacity_/4) {
                        // if loading is > 75% then resize the set and call back to insert to place element
                        resize_(2*capacity_);
                        insert(forward<K>(key));
                    } else {
                        // otherwise just build the new hash bucket as requested
                        storage_[pos].hash = hash;
                        new (&storage_[pos].key) K(forward<K>(key));
                        used_++;
                    }
                    break;

                } else if (!swapped && storage_[pos].hash == hash && (storage_[pos].key == key)) {
                    // found a bucket that matches our key, so set already contains it
                    break;
                    
                } else {
                    // found non-empty bucket that doesn't match us. If it has a probe distance
                    // less than ours, we'll swap with it and continue looking to place the element
                    size_t newdist = probedist(storage_[pos].hash, pos);
                    if (newdist < curdist) {
                        curdist = newdist;
                        swap(storage_[pos].hash,   hash);
                        swap(storage_[pos].key, key);
                        swapped = true;
                    }
                }
            }

            return *this;
        }
            
        
        // lvalue overload for insert operator
        template <class K>
        hashset<K>& hashset<K>::insert(const K& key) { insert(std::move(K(key)));  return *this; }

        
        // resize the container to the new given size.  This involves re-hashing and moving
        // all the elements in the set to new memory.  Always resizes to next power of 2 above requested
        template <class K>
        void hashset<K>::resize_(size_t desired) {
            // find next higher power of 2
            size_t newsize = 1;
            while (newsize < desired) newsize *= 2;

            // quit if there's no change
            if (newsize == capacity_) {
                return;
            }
            
            // save old pointer and capacity
            bucket *old_storage  = storage_;
            size_t  old_capacity = capacity_;
            size_t  old_used     = used_;
            
            // reset the set to be empty with new capacity
            used_     = 0;
            capacity_ = newsize;
            storage_  = new bucket[capacity_];

            // rehash existing elements
            size_t cnt=0;
            for (size_t ii=0; ii < old_capacity; ii++) {
                if (cnt == old_used) break;

                if (old_storage[ii].hash != EMPTY_BUCKET) {
                    insert(std::move(old_storage[ii].key));
                    old_storage[ii].hash = EMPTY_BUCKET;
                    cnt++;
                }
            }

            delete [] old_storage;            
        }
        

        // search for a given key in the set, returns index if found, negative value if not
        template <class K>
        ssize_t hashset<K>::search_(const K& key) const {
            size_t hash = hash_key(key);
            
            for (size_t off=0; off < capacity_; off++) {
                size_t pos = modulo(hash + off, capacity_);

                // common case is that we hit the right bucket right away
                if ((storage_[pos].hash   == hash) &&
                    (storage_[pos].key == key)) {
                    return pos;
                    
                // otherwise if we hit an empty bucket or one that's
                // been displaced less than we've searched, we're done
                } else
                    if ((storage_[pos].hash == EMPTY_BUCKET) ||
                        (probedist(storage_[pos].hash, pos) < off)) {
                        break;
                    }
            }
            return -1;
        }

        
        // return true if the set contains a given key
        template <class K>
        bool hashset<K>::has(const K &key) const {
            return (search_(key) >= 0);
        }


        // remove an element from the set, does nothing if key doesn't exist
        template <class K>
        void hashset<K>::remove(const K& key) {
            ssize_t ii = search_(key);
            if (ii < 0) return;
            
            // delete found element
            storage_[ii].hash = EMPTY_BUCKET;
            storage_[ii].key.~K();
            
            // backshift elements to reduce variance
            size_t prev;
            for (size_t off=0; off < capacity_; off++) {
                prev = ii;
                ii   = modulo(ii+1, capacity_);

                if ((storage_[ii].hash == EMPTY_BUCKET) ||
                    (probedist(storage_[ii].hash, ii) == 0)) {
                    storage_[prev].hash = EMPTY_BUCKET; // nothing to move into last bucket
                    break;
                }
                
                // move bucket into previous bucket and destruct
                storage_[prev].hash = storage_[ii].hash;
                new (&storage_[prev].key) K(std::move(storage_[ii].key));
                storage_[ii].key.~K();
            }
            
            used_--;
        }


        // clear the container of all elements
        template <class K>
        void hashset<K>::clear() {
            used_ = 0;

            for (size_t ii=0; ii < capacity_; ii++) {
                if (storage_[ii].hash != EMPTY_BUCKET) {
                    storage_[ii].hash  = EMPTY_BUCKET;
                    storage_[ii].key.~K();
                }
            }
        }


        // resize set to minimum size given current number of entries
        template <class K>
        void hashset<K>::compact() {
            resize_(used_+1); // +1 because we can never have used_ == capacity_ or we'll never find an empty slot during search 
        }


        // return true if this is a subset of other
        template <class K>
        bool hashset<K>::issubset(const hashset<K> &other) const {
            bool subset=true;

            size_t cnt=0;
            for (size_t ii=0; ii < capacity_ && subset; ii++) {
                if (cnt >= used_) break;
                
                if (storage_[ii].hash != EMPTY_BUCKET) {
                    subset &= other.has(storage_[ii].key);
                    cnt++;
                }
            }

            return subset;
        }


        // return true if this is a superset of other
        template <class K>
        bool hashset<K>::issuperset(const hashset<K> &other) const {
            return other.issubset(*this);
        }
    } // namespace {
} // namespace prelude {
