// -*-c++-*-
#pragma once

// contains an implementation of a hashmap.  Maps keys and values to each other using a hashing function.
// uses the Robin-Hood algorithm for a significant performance boost over std::unordered_map on average.
// inspired by, but not constraining to be limited by, python's dict class.
 
#include <cstdlib>
#include <cstdint>

#include <limits> // std::numeric_limits
#include <initializer_list>

#include <prelude/maybe.h>
#include <prelude/hashing.h>

namespace prelude { 
    namespace  { // interior namespace to prevent external visibility
        template <class K, class V>
        struct hashmap {
            static const size_t EMPTY_BUCKET = std::numeric_limits<size_t>::max();
            static const size_t DEFAULT_SIZE = 8;

            // convenience typedefs
            typedef K key_type;
            typedef V val_type;
            
            // type containing key/value pair
            struct keyval {
                K key;
                V val;
            };

            hashmap();                                                // default constructor
            hashmap(std::initializer_list<std::pair<K,V>> list);      // initializer list constructor
            hashmap(const hashmap&  other);                           // copy constructor
            hashmap(      hashmap&& other);                           // move constructor
           ~hashmap();                                                // destructor

            hashmap&  operator  =(hashmap other);                     // copy-and-swap assignment operator
            maybe<V&> operator [](const K& key) const;               

            bool          has    (const K& key) const;                // return true if a given key is in the table
            V             get    (const K& key, const V& def) const;  // return value associated with key if it exists, otherwise def

            hashmap<K,V>& insert (      K&&  key,       V&& val);     // insert new key-value into the table, 4 overloads for various value classes
            hashmap<K,V>& insert (const K&   key,       V&& val);     // if key exists, update with given value
            hashmap<K,V>& insert (      K&&  key, const V&  val);      
            hashmap<K,V>& insert (const K&   key, const V&  val);      

            void          remove (const K& key);                      // erase key-value from the table if it exists
            void          clear  ();                                  // clear the container of all entries
            void          compact();                                  // rehash table to minimum size given current entries
            void          update (const hashmap<K,V>&  other);        // update table with entries from other table
            void          update (const hashmap<K,V>&& other);        // update table with entries from other table that can be moved from
            
            bool          empty()    const { return size() == 0; }    // return true if table is empty
            size_t        size ()    const { return used_;       }    // return number of elements in table
                            
        private:
            void          resize_(size_t);                            // function to resize the hash table
            ssize_t       search_(const K& key) const;                // search for a particular key in the hashtable

            
            // modulo function for wrapping hash value back into bucket, we
            // limit ourselves to power-of-2 sizes, so we can use & instead of %
            static inline size_t modulo(size_t hash, size_t cap) {
                return hash & (cap-1);
            }

            
            // compute hash for instance of key, don't use the empty sentinel as a hash value
            static inline size_t hash_key(const K& key) {
                size_t hash = digest(key);
                if (hash == EMPTY_BUCKET) {
                    hash++;
                }
                return hash;
            }

            
            // bucket type, holds a hash, and a key-value pair.
            // union type gives us aligned storage for keyval without initializing it.
            struct bucket {  
                bucket() : hash(EMPTY_BUCKET) {}
               ~bucket() {
                   // if bucket isn't empty, have to destruct key and value explicitly
                   if (hash != EMPTY_BUCKET) {
                       kv.key.~K();
                       kv.val.~V();
                   }
                }

                // union provides aligned, but unitialized, storage for keyval
                size_t hash;
                union {
                    keyval kv;
                };
            };

            
            // compute displacement of a given hash found at index from its ideal location
            size_t probedist(size_t hash, size_t idx) const {
                size_t desired = modulo(hash, capacity_);

                if (desired <= idx) {
                    return idx - desired;
                } else {
                    // index wrapped around, return circular distance
                    return (capacity_ - desired) + idx;
                }
            }

            
            size_t  capacity_;
            size_t  used_;
            bucket *storage_;

            
            template <class KK, class VV>
            friend void swap(hashmap<KK,VV> &flip, hashmap<KK,VV> &flop);
        };

        
        //-------------------------------------------------------------------------------
        // hashmap implementation
        //-------------------------------------------------------------------------------   

        // swap operation
        template <class K, class V>
        void swap(hashmap<K,V> &flip, hashmap<K,V> &flop) {
            using std::swap;
            swap(flip.capacity_, flop.capacity_);
            swap(flip.used_,     flop.used_    );
            swap(flip.storage_,  flop.storage_ );
        }


        // default constructor 
        template <class K, class V>
        hashmap<K,V>::hashmap()
            : capacity_(0), used_(0), storage_(nullptr) {}
        

        // initializer list constructor
        template <class K, class V>
        hashmap<K,V>::hashmap(std::initializer_list<std::pair<K,V>> list)
            : hashmap() {
            for (auto item : list) {
                insert(std::move(item.first), std::move(item.second));
            }
        }

        
        // destructor
        template <class K, class V>
        hashmap<K,V>::~hashmap() {
            delete [] storage_; // storage destructor will handle destructing any key/values we have
            storage_ = nullptr;
        }    

    
        // copy constructor
        template <class K, class V>
        hashmap<K,V>::hashmap(const hashmap& other)
            : capacity_(other.capacity_), used_(other.used_), storage_(new bucket[capacity_]) {

            // have to construct key/values via placement new rather than assigning them
            // because default constructor for bucket will not give us a valid object to assign to.
            // note we iterate over capacity_ but break once we've seen used_ elements.  This is
            // because elements are (almost surely) non-contiguous.            
            size_t cnt=0;
            for (size_t ii=0; ii < capacity_; ii++) {
                if (cnt == other.used_) break;
                
                size_t hash = storage_[ii].hash = other.storage_[ii].hash;
                if (hash != EMPTY_BUCKET) { 
                    new (&storage_[ii].kv.key) K(other.storage_[ii].kv.key);
                    new (&storage_[ii].kv.val) V(other.storage_[ii].kv.val);
                    cnt++;
                }
            }
        }
        

        // move constructor
        template <class K, class V>
        hashmap<K,V>::hashmap(hashmap&& other) {
            capacity_       = other.capacity_;
            used_           = other.used_;
            storage_        = other.storage_;
            other.storage_  = nullptr;
            other.capacity_ = 0;
            other.used_     = 0;
        }

        
        // assignment operator, uses copy-and-swap trick
        // in the event of a an rvalue reference parameter passed, hashmap will be built using
        // its move constructor, and then swapped into place, so we don't need a separate
        // move assignment operator.
        template <class K, class V>
        hashmap<K,V>& hashmap<K,V>::operator =(hashmap other) {
            swap(*this, other);
            return *this;
        }
        
        
        // insert operator.  If the key doesn't exist in the table, build it with
        // the given value.  If it does exist, overwrite it with new value.  Other
        // insert variants wrap this with appropriate forward/move semantics
        template <class K, class V>
        hashmap<K,V>& hashmap<K,V>::insert(K&& key, V&& val) {
            using std::swap; // for ADL
            using std::move;
            using std::forward;

            if (capacity_ == 0) {
                resize_(DEFAULT_SIZE);
            }
            
            size_t hash = hash_key(key);
            size_t idx  = modulo(hash, capacity_);
           
            // scan through container until we find an empty bucket.  If we find the key
            // along the way, just set its value and we're done.  If we find any elements
            // with a smaller probe distance than ours, swap with them and continue loop
            bool swapped=false;
            for (size_t ii=0, curdist=0;  ii < capacity_;  ii++, curdist++) {
                size_t pos = modulo(idx + ii, capacity_);

                if (storage_[pos].hash == EMPTY_BUCKET) {
                    // found an empty bucket, place what we currently have there
                    if (used_ >= 3*capacity_/4) {
                        // if loading is > 75% then resize the table and call back to insert to place element
                        resize_(2*capacity_);
                        insert(forward<K>(key), forward<V>(val));
                    } else {
                        // otherwise just build the new hash bucket as requested
                        storage_[pos].hash = hash;
                        new (&storage_[pos].kv.key) K(forward<K>(key));
                        new (&storage_[pos].kv.val) V(forward<V>(val));
                        used_++;
                    }
                    break;

                } else if (!swapped && storage_[pos].hash == hash && (storage_[pos].kv.key == key)) {
                    // found a bucket that matches our key, update it
                    storage_[pos].kv.val = forward<V>(val);
                    break;
                    
                } else {
                    // found non-empty bucket that doesn't match us. If it has a probe distance
                    // less than ours, we'll swap with it and continue looking to place the element
                    size_t newdist = probedist(storage_[pos].hash, pos);
                    if (newdist < curdist) {
                        curdist = newdist;
                        swap(storage_[pos].hash,   hash);
                        swap(storage_[pos].kv.key, key);
                        swap(storage_[pos].kv.val, val);
                        swapped = true;
                    }
                }
            }

            return *this;
        }
            

        // insert variants for different value classes
        template <class K, class V>
        hashmap<K,V>& hashmap<K,V>::insert(const K&  key,       V&& val) { insert(std::move(K(key)),    std::forward<V>(val)); return *this; }

        template <class K, class V>
        hashmap<K,V>& hashmap<K,V>::insert(      K&& key, const V&  val) { insert(std::forward<K>(key), std::move(V(val)));    return *this; }

        template <class K, class V>
        hashmap<K,V>& hashmap<K,V>::insert(const K&  key, const V&  val) { insert(std::move(K(key)),    std::move(V(val)));    return *this; }


        // update table using values from another hash table
        template <class K, class V>
        void hashmap<K,V>::update(const hashmap<K,V>& other) {
            size_t cnt=0;
            for (size_t ii=0; ii < other.capacity_; ii++) {
                if (cnt >= other.used_) break;

                if (other.storage_[ii].hash != EMPTY_BUCKET) {
                    insert(other.storage_[ii].kv.key, other.storage_[ii].kv.val);
                    cnt++;
                }
            }
        }

        
        // update table using values from another hash table using move semantics
        template <class K, class V>
        void hashmap<K,V>::update(const hashmap<K,V>&& other) {
            size_t cnt=0;
            for (size_t ii=0; ii < other.capacity_; ii++) {
                if (cnt >= other.used_) break;

                if (other.storage_[ii].hash != EMPTY_BUCKET) {
                    insert(std::move(other.storage_[ii].kv.key), std::move(other.storage_[ii].kv.val));
                    cnt++;
                }
            }
        }
 

        // resize the container to the new given size.  This involves re-hashing and moving
        // all the elements in the hashtable to new memory.  Always rounds up to next higher power of 2
        template <class K, class V>
        void hashmap<K,V>::resize_(size_t desired) {
            // find next higher power of 2
            size_t newsize = 1;
            while (newsize < desired) newsize *= 2;

            if (newsize == capacity_) {
                return;
            }
            
            // save old pointer and capacity
            bucket *old_storage  = storage_;
            size_t  old_capacity = capacity_;
            size_t  old_used     = used_;
            
            // reset the hash table to be empty with new capacity
            used_     = 0;
            capacity_ = newsize;
            storage_  = new bucket[capacity_];

            // rehash existing elements
            size_t cnt=0;
            for (size_t ii=0; ii < old_capacity; ii++) {
                if (cnt == old_used) break;

                if (old_storage[ii].hash != EMPTY_BUCKET) {
                    insert(std::move(old_storage[ii].kv.key), std::move(old_storage[ii].kv.val));
                    old_storage[ii].hash = EMPTY_BUCKET;
                    cnt++;
                }
            }

            delete [] old_storage;            
        }
        
        
        // search for a given key in the hash table, returns index if found, negative value if not
        template <class K, class V>
        ssize_t hashmap<K,V>::search_(const K& key) const {
            size_t hash = hash_key(key);
            
            for (size_t off=0; off < capacity_; off++) {
                size_t pos = modulo(hash + off, capacity_);

                // common case is that we hit the right bucket right away
                if ((storage_[pos].hash   == hash) &&
                    (storage_[pos].kv.key == key)) {
                    return pos;
                    
                // otherwise if we hit an empty bucket or one that's
                // been displaced less than we've searched, we're done
                } else
                    if ((storage_[pos].hash == EMPTY_BUCKET) ||
                        (probedist(storage_[pos].hash, pos) < off)) {
                        break;
                    }
            }
            return -1;
        }


        // lookup an element by the key. returns an invalid maybe if not found
        template <class K, class V>
        maybe<V&> hashmap<K,V>::operator[](const K &key) const {
            ssize_t ii = search_(key);
            if (ii >= 0) {
                return storage_[ii].kv.val;
            } else {
                return maybe_error();
            }
        }

        
        // return true if the table contains a given key
        template <class K, class V>
        bool hashmap<K,V>::has(const K &key) const {
            return (search_(key) >= 0);
        }

        
        // return the value associated with key if it's in the table, otherwise return given default
        template <class K, class V>
        V hashmap<K,V>::get(const K& key, const V &def) const {
            ssize_t ii = search_(key);
            if (ii >= 0) {
                return storage_[ii].kv.val;
            }
            return def;
        }


        // remove an element from the table, does nothing if key doesn't exist
        template <class K, class V>
        void hashmap<K,V>::remove(const K& key) {
            ssize_t ii = search_(key);
            if (ii < 0) return;
            
            // delete found element
            storage_[ii].hash = EMPTY_BUCKET;
            storage_[ii].kv.key.~K();
            storage_[ii].kv.val.~V();
            
            // backshift elements to reduce variance
            size_t prev;
            for (size_t off=0; off < capacity_; off++) {
                prev = ii;
                ii   = modulo(ii+1, capacity_);

                if ((storage_[ii].hash == EMPTY_BUCKET) ||
                    (probedist(storage_[ii].hash, ii) == 0)) {
                    storage_[prev].hash = EMPTY_BUCKET; // nothing to move into last bucket
                    break;
                }
                
                // move bucket into previous bucket and destruct
                storage_[prev].hash = storage_[ii].hash;
                new (&storage_[prev].kv.key) K(std::move(storage_[ii].kv.key));
                new (&storage_[prev].kv.val) V(std::move(storage_[ii].kv.val));
                storage_[ii].kv.key.~K();
                storage_[ii].kv.val.~V();
            }
            
            used_--;
        }


        // clear the container of all elements
        template <class K, class V>
        void hashmap<K,V>::clear() {
            used_ = 0;

            for (size_t ii=0; ii < capacity_; ii++) {
                if (storage_[ii].hash != EMPTY_BUCKET) {
                    storage_[ii].hash  = EMPTY_BUCKET;
                    storage_[ii].kv.key.~K();
                    storage_[ii].kv.val.~V();
                }
            }
        }


        // resize table to minimum size given current number of entries
        template <class K, class V>
        void hashmap<K,V>::compact() {
            resize_(used_+1); // +1 because we can never have used_ == capacity_ or we'll never find an empty slot during search 
        }
    } // namespace {
} // namespace prelude {
