//-*-c++-*-
#pragma once

#include <cmath>
#include <algorithm> // min/max

//
// by default, uses the stable algorithms for complex division and magnitude, if these don't matter,
// #define CX_FAST_MATH before including to use the naive algorithms

// some macros to make thing shorter
#define SCTYPET  template <typename T> static inline
#define SCTYPETB template <typename T, typename B> static inline

namespace prelude {
    namespace {
        // function prototypes
        template <typename T> struct cplxnum;
        template <typename T> cplxnum<T> cdiv  (cplxnum<T> a, cplxnum<T> b); // naive division
        template <typename T> cplxnum<T> cdivbs(cplxnum<T> a, cplxnum<T> b); // baudin-smith division

        // complex class proper
        template <class T>
        struct cplxnum {
            typedef T type;

            cplxnum() = default; // give us intrinsic initialization semantics in c++11
            // cfloat x;                    has undefined values
            // auto x = cfloat();           is zero initialized
            // auto x = new cfloat[1024];   is undefined
            // auto x = new cfloat[1024](); is zero initialized

            // promote from real/construct with re/im
            cplxnum(T re, T im = T())
                : re(re), im(im)  {}
    
            // gcc-4.4.7 generates very poor code without an explicit copy constructor (bug?)
            cplxnum(const cplxnum& other)
                : re(other.re), im(other.im) {}
    
            // converting constructor, doesn't count as copy constructor
            template <class B>
            cplxnum(const cplxnum<B>& other)
                : re(T(other.re)), im(T(other.im)) {}
    
            // with a copy constructor, disjoint fields are OK on all compilers tested
            T re, im;
        };

        // operators need to specify cplx*cplx, scalar*cplx and cplx*scalar

        // unary +/-
        SCTYPET   cplxnum<T> operator -(cplxnum<T> a) { return cplxnum<T>(-a.re, -a.im); }
        SCTYPET   cplxnum<T> operator +(cplxnum<T> a) { return a;                        }

        // equality comparison
        SCTYPETB  bool       operator==(cplxnum<T> a, cplxnum<B> b) { return (a.re == b.re) && (a.im == b.im); }
        SCTYPETB  bool       operator==(cplxnum<T> a, B          b) { return (a.re == b)    && (a.im == T());  }
        SCTYPETB  bool       operator==(T a,          cplxnum<B> b) { return (a    == b.re) && (T()  == b.im); }

        SCTYPETB  bool       operator!=(cplxnum<T> a, B          b) { return !(a == b);                        }
        SCTYPETB  bool       operator!=(T a,          cplxnum<B> b) { return !(a == b);                        }
        SCTYPETB  bool       operator!=(cplxnum<T> a, cplxnum<B> b) { return !(a == b);                        }


        // promoting operators
#define CPROMAB template <typename A, typename B> static inline 

        CPROMAB auto operator +(cplxnum<A> a, B b)          -> cplxnum<decltype(A()+B())> { return cplxnum<decltype(A()+B())>(a.re + b,    a.im);         }
        CPROMAB auto operator +(A a,          cplxnum<B> b) -> cplxnum<decltype(A()+B())> { return cplxnum<decltype(A()+B())>(a    + b.re, b.im);         }
        CPROMAB auto operator +(cplxnum<A> a, cplxnum<B> b) -> cplxnum<decltype(A()+B())> { return cplxnum<decltype(A()+B())>(a.re + b.re, a.im + b.im);  }

        CPROMAB auto operator -(cplxnum<A> a, B b)          -> cplxnum<decltype(A()-B())> { return cplxnum<decltype(A()-B())>(a.re - b,     a.im);        }
        CPROMAB auto operator -(A a,          cplxnum<B> b) -> cplxnum<decltype(A()-B())> { return cplxnum<decltype(A()-B())>(a    - b.re, -b.im);        }
        CPROMAB auto operator -(cplxnum<A> a, cplxnum<B> b) -> cplxnum<decltype(A()-B())> { return cplxnum<decltype(A()-B())>(a.re - b.re,  a.im - b.im); }

        CPROMAB auto operator *(cplxnum<A> a, B b)          -> cplxnum<decltype(A()*B())> { return cplxnum<decltype(A()*B())>(a.re*b, a.im*b);            }
        CPROMAB auto operator *(A a,          cplxnum<B> b) -> cplxnum<decltype(A()*B())> { return cplxnum<decltype(A()*B())>(a*b.re, a*b.im);            }
        CPROMAB auto operator *(cplxnum<A> a, cplxnum<B> b) -> cplxnum<decltype(A()*B())> {
            return cplxnum<decltype(A()*B())>(
                a.re*b.re - a.im*b.im,
                a.re*b.im + a.im*b.re
            );
        }

        CPROMAB auto operator /(cplxnum<A> a, B b)          -> cplxnum<decltype(A()/B())> { return cplxnum<decltype(A()/B())>(a.re/b, a.im/b);                    }
        CPROMAB auto operator /(A a,          cplxnum<B> b) -> cplxnum<decltype(A()/B())> { return cplxnum<decltype(A()/B())>(a) / cplxnum<decltype(A()/B())>(b); }
        CPROMAB auto operator /(cplxnum<A> a, cplxnum<B> b) -> cplxnum<decltype(A()/B())> {
#ifdef CX_FAST_MATH
            return cdiv  (cplxnum<decltype(A()/B())>(a), cplxnum<decltype(A()/B())>(b)); // fast grade-school algorithm
#else
            return cdivbs(cplxnum<decltype(A()/B())>(a), cplxnum<decltype(A()/B())>(b)); // accurate baudin-smith algorithm
#endif
        }

#undef CPROMAB

        // in-place operators
        SCTYPETB cplxnum<T>& operator +=(cplxnum<T> &a, B b) { return a = a+b; }
        SCTYPETB cplxnum<T>& operator -=(cplxnum<T> &a, B b) { return a = a-b; }
        SCTYPETB cplxnum<T>& operator *=(cplxnum<T> &a, B b) { return a = a*b; }
        SCTYPETB cplxnum<T>& operator /=(cplxnum<T> &a, B b) { return a = a/b; }

        // basic utility functions
        SCTYPET T abs_sq(cplxnum<T> a) { return a.re*a.re + a.im*a.im; }
        SCTYPET T abs   (cplxnum<T> a) {
#ifdef CX_FAST_MATH
            // sqrt is inlined in all tested compilers with optimization on
            return sqrt(a.re*a.re + a.im*a.im);
#else
            // inline hypot function, none of the compilers tested inline it without --ffast-math
            if (a.re == 0 && a.im == 0) {
                return 0;
            }
            
            T t;
            T x = fabs(a.re);
            T y = fabs(a.im);
            t = std::min(x,y); // fmin/fmax do not get inlined in icc or gcc
            x = std::max(x,y);
            t = t/x;
            return x*sqrt(1+t*t);
#endif
        }
    
        SCTYPET T          phase (cplxnum<T> a) { return atan2(a.im, a.re);                             }
        SCTYPET cplxnum<T> conj  (cplxnum<T> a) { return cplxnum<T>(a.re, -a.im);                       }
        SCTYPET cplxnum<T> unit  (cplxnum<T> a) { T mag = abs(a); if (mag != 0) return a/mag; return a; }


        // naive division
        template <typename T>
        cplxnum<T> cdiv(cplxnum<T> a, cplxnum<T> b) {
            return a*conj(b)/abs_sq(b);
        }


        // baudin-smith division
        template <typename T>
        cplxnum<T> cdivbs(cplxnum<T> x, cplxnum<T> y) {
            // use Baudin-Smith algorithm for complex division
            // see "A Robust Complex Division in Scilab"
            //
            // this actually isn't the absolute positive best accuracy you can
            // get (see the paper), but it improves on basic smith and it's
            // not __too__ computationally intensive.
            struct h {
                static inline void internal(T a, T b, T c, T d, T& e, T &f) {
                    T r = d/c;        
                    T t = 1/(c + d*r);
                    if (r != 0) {
                        e = (a + b*r) * t;
                        f = (b - a*r) * t;
                    } else {
                        e = (a + d*(b/c)) * t; 
                        f = (b - d*(a/c)) * t;
                    }
                }
            };
    
            T a = x.re; T b = x.im;
            T c = y.re; T d = y.im;
            T e,f;

            // fabs seems to get inlined in every compiler tested
            if (fabs(d) <= fabs(c)) {
                h::internal(a,b,c,d,e,f);
            } else {
                h::internal(b,a,d,c,e,f);
                f = -f;
            }

            return cplxnum<T>(e,f);
        }

        
        // compute e^(jx) as a complex value
        template <typename T>
        cplxnum<T> expj(T xx) {
            cplxnum<T> ans;
            sincos(xx, &ans.im, &ans.re);
            return ans;
        }


        // multiply complex number by j (rotate)
        template <typename T>
        cplxnum<T> mulj(cplxnum<T> xx) {
            return cplxnum<T>(-xx.im, xx.re);
        }


        // convenience typedefs
        typedef cplxnum<float>       cfloat;
        typedef cplxnum<double>      cdouble;
        typedef cplxnum<long double> cldouble;

        // constant for j
        const cldouble Cj = cdouble(0.0, 1.0);
    } // interior namespace
} // namespace prelude

// cleanup our pre-processor mess
#undef SCTYPET
#undef SCTYPETB
