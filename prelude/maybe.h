#pragma once

// Copyright (c) 2017, Sean McAllister
// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <string>
#include <exception>
#include <algorithm> // std::swap

// contains an implementation of a "maybe" container that can be used to model
// function return failure.  This is useful in cases where it's expensive or
// impractical to check if a function can succeed before calling it, and allows
// for friendlier semantics than exceptions.  maybe requires copy/move constructibility
// but not default constructibility.
//
// If we have a function that returns a maybe type:
//     maybe<double> safe_sqrt(double val) {
//         if (val >= 0) {
//             return sqrt(val);
//         }
//         return maybe_error("square root of negative number");
//     }
//
// Then we can safely call the function with an input that fails
// as long as we check the return value.  If we know the call is safe, the check
// can be omitted:
//
// double val = safe_sqrt(3.1415);
// double sum = val + 1;
//
// otherwise, we can check for validity of the result before continuing:
//
// if (auto val = safe_sqrt(-3.1415)) {
//     double sum = val + 1;
// }
//
// If an invalid value is used, an error message is printed and the program
// is terminated via std::exit().  If you can accomodate this (eg: in a top-level
// program), you can just assume that fallible operations succeed, much
// like an exception:
//
// rawfile file = rawfile::openr("somefile.dat"); // returns maybe<rawfile>
// file.write("this is a test");
// file.close();
//
// If the file fails to open, extracting it from the maybe (by assigning to file) will
// fail and the program will terminate, so no later code is executed.
//
// To indicate failure, you return an instance of maybe_error.  This can either be
// default constructed, or take a string as a parameter.  In the latter case,
// a thread_local string is set to contain the error, so no additional space
// is required in the maybe instance for the string itself.
// 
// maybe versions that support value and reference types are provided.
//

// anonymous namespace prevents external symbol visibility
namespace {
    // hold optional error string from last maybe error
    thread_local std::string maybe_err_str;

    // print error if we have one
    static inline void maybe_print_error() {
        if (maybe_err_str != "") {
            fprintf(stderr, "maybe<> failure - %s\n", maybe_err_str.c_str());
        } else {
            fprintf(stderr, "accessed invalid maybe<> instance\n");
        }
    }
    

    // sentinel for error condition, handles setting global error string value
    struct maybe_error {
        maybe_error()                { maybe_err_str = "";  }
        maybe_error(std::string err) { maybe_err_str = err; }
    };

    
    // maybe implementation for non-reference types
    template <class T>
    struct maybe {
        maybe()             : valid(false) {}  // default constructor
        maybe(maybe_error)  : valid(false) {}  // construct from type sentinel
        maybe(const T& val) : valid(true)  {   // value constructor
            new (&value) T(val);
        }

        maybe(T&& val)      : valid(true) {    // rvalue constructor
            new (&value) T(std::forward<T>(val));
        }

        maybe(const maybe &other) {            // copy constructor
            valid = other.valid;
            if (valid) {
                new (&value) T(other.value);
            }
        }

        maybe(maybe &&other) {                 // move constructor
            valid = other.valid;
            other.valid = false;
            value = std::move(other.value);
        }

        ~maybe() {                             // destructor
            if (valid) {
                value.~T();
            }
        }


        // assign from another maybe container
        // uses copy-and-swap and handles the rvalue assignment too
        maybe& operator =(maybe other) {
            swap(*this, other);
            return *this;
        }


        // type-coercion operator to get back to wrapped value
        operator T() {
            if (!valid) {
                maybe_print_error();
                exit(EXIT_FAILURE);
            }
            return value;
        }

        // for checking validity of container
        explicit operator bool() { return valid; }
    private:
        // aligned but uninitialized memory for a T
        union {
            T value;
        };

        // note that this comes after the value storage.  This is important
        // as it will prevent a single instance from having to be padded to
        // meet the alignment requirements of T.  Eg: if T=double, then having
        // a 1-byte field first requires that we pad 7 bytes before T itself,
        // which is a waste.
        bool valid;

        // swap implementation, global swap will call this
        static void swap(maybe &flip, maybe &flop) {
            using std::swap;
            swap(flip.valid, flop.valid);
            swap(flip.value, flop.value);
        }

        // make us a friend of global swap so we can call above swap
        template <class TT> friend void swap(maybe<TT> &flip, maybe<TT> &flop);
    };



    // implementation enabled for reference types
    template <class T>
    struct maybe<T&> {
        maybe()            : value(nullptr) {}  // default constructor
        maybe(maybe_error) : value(nullptr) {}  // construct from type sentinel
        maybe(T& val)      : value(&val)    {}  // value constructor

        // assign to contained reference
        maybe& operator =(const T &other) {
            *value = other;
            return *this;
        }

        // retrieve reference through coercion operator
        operator T&() {
            if (!value) {
                maybe_print_error();
                exit(EXIT_FAILURE);
            }
            return *value;
        }

        explicit operator bool() { return value != nullptr; }
    private:
        T* value;

        // swap implementation, global swap will call this
        static void swap(maybe &flip, maybe &flop) {
            using std::swap;
            swap(flip.value, flop.value);
        }

        // make us a friend of global swap so we can call above swap
        template <class TT> friend void swap(maybe<TT> &flip, maybe<TT> &flop);
    };


    // rvalue references cannot be used with maybe
    // have to do the falsity pass through to get to second phase expansion before assertion is thrown
    template <class T>
    struct maybe<T&&> {
        template <class TT> struct falsity {
            static const bool value = false;
        };
        static_assert(falsity<T>::value, "cannot use rvalue reference with maybe");
    };


    // swap for maybe class, just defers to private swap defined in class
    template <class T>
    void swap(maybe<T> &flip, maybe<T> &flop) {
        maybe<T>::swap(flip, flop);
    }
} // namespace {
